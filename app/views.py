from flask import Flask
from app import app
from app.models import User
from app.database import db_session

u = User('admin', 'mail@admin.com')
db_session.add(u)
db_session.commit()

@app.route('/')
def hello():
    return "Hello World!"

@app.route('/db')
def hello_db():
    user = User.query.filter(User.name == 'admin').first()
    return "Hello {}!".format(user.name)

@app.route('/<name>')
def hello_name(name):
    return "Hello {}!".format(name)
