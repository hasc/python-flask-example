from flask import Flask
from app.database import init_engine, init_db

# CREATE APP OBJECT
## app the variable...
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'

init_engine(app.config['SQLALCHEMY_DATABASE_URI'], convert_unicode=True)
init_db()

# IMPORT views MODULE
from app import views


# The import statement is at the end to
# avoid circular references.

# The views module needs the app variable
# defined in this script.
