FROM python:3.7-alpine

RUN addgroup -S uwsgi && adduser -S -G uwsgi uwsgi

COPY requirements.txt /
RUN set -e; \
	apk add --no-cache --virtual .build-deps \
		gcc \
		libc-dev \
		linux-headers \
	; \
	pip install -r requirements.txt; \
	apk del .build-deps;


COPY app/ /app
RUN chown -R uwsgi:uwsgi /srv

WORKDIR /app
EXPOSE 5000

CMD uwsgi --http :5000  --manage-script-name --mount /=app:app --enable-threads --processes 1 --uid uwsgi --gid uwsgi