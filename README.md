python-flask-example
====================
An example application based on the Python's [Flask](http://flask.pocoo.org/) framework.

## Application Endpoints
The following endpoints are provided:
* <http://localhost:5000/>


## Development

### Running the application locally

```sh
cd app
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
flask run
```

## Docker

### Build the docker image
```sh
docker build -t python-flask-sample:latest
```

### Run the docker container

```sh
docker run -p 5000:5000 python-flask-sample
```

The application should be running under <http://localhost:5000>